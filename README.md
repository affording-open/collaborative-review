Collaborative scientific review
===============================

Status: drafting

Some requirements for science (see Karl Popper) are to be able to
notice errors, to remember them and to updated what those newly found
errors mean for all other knowledge. This is something that many
humans do today in practice as individuals or in small groups. All the
knowledge available to humans is too much to review for errors by a
small group. Collaborating on that with bigger groups of people faces
various challenges. While science progresses despite these challenges,
I'd prefer to have the option of easier collaboration. This is trying
to describe a mechanism for that which can be implemented in
software. Many parts of it are things that humans already do
consciously or possibly subconsciously, however it can only scale with
the use of computers.

This proposed mechanism is not limited to scientific assertions, it
can be applied to many processes or review goals. Though for this
mechanism to work properly, there are some necessary qualities from
the goal to review.

None of the ideas in here are new, but I have not found them combined
anywhere in an implementation, yet.

Terminology and base structure
------------------------------

* A source can be something that can be repeatably perceived by a
  human and its content can be addressed and copied by a computer as a
  whole.
* For some sources, parts of them can be computer addressed (e.g. the
  third sentence in the first paragraph of a text; or byte 128-150 of
  a text.)
* An assertion is about an addressable source.
* A review is an assertion on an assertions. It identifies its
  author.
  Examples:
  - "Alice" reviewed the paper "Foo" for "errors in the statistics it
    uses" and found "none".
  - "Alice" reviewed the paper "Foo" and found an "error": "The
    significance level used is not sufficient for this situation as
    explained in the paper Statistical testing for Foo by Bob."
  - "Alice" reviewed the paper "Foo" and found an "correctable error"
    in the "6th word of the 3rd sentence" by replacing it with
    "three": "The conclusion of the paper says it is three times as
    much not four times."
* When person or author is mentioned here it refers to a cryptographic
  identity.
* A way needs to be provided to correct yourself.

TODO more formal and also more examples

Some assumptions:

* This mechanism is only intended to work on assertions that also have
  an assertion that it targets the review goal. (To assert
  a source is intended to be e.g. scientific.) Which is not the same
  as an assertion that some source is true. The first and second
  assertion do not need to be from the same person.
* While it is possible to adapt this to a changing goal, it is at
  first assumed the review goal is stable. One of the adaptions can be
  a temporal qualifications.
* TODO there are probably more.

Some notable details that follow from the base structure:

* Reviews can be made of reviews. (See
  [reification](https://en.wikipedia.org/wiki/Reification_(knowledge_representation)).)
* How much I trust someone (predicting how likely their assertions are
  true) can be calculated based on my reviews of that persons
  assertions. Thus computing a web of trust without specifying the
  trust values directly.

Requirements
------------

* While looking for assertions to review, assertions with sufficient
  reviews by people I trust get lower priority. Assertions by people I
  trust negatively get even lower priority.
* You can look for assertions that are likely to contradict the
  current understanding my trusted peers and me built. E.g. by
  prioritising those that your web of trust disagrees more about.
* You can look for assertions from yourself that likely contradict
  other assertions by you.
* You can look at all assertions for a source that are not negatively
  trusted.
* While looking for likely true assertions, you can find those that
  were reviewed positively by people you trust.
* How much you trust a person is based on your review of their
  assertions.
* To support different specializations, trust is calculated
  individually for different predicates. E.g. you can trust someone to
  review statistics without trusting the same person to review
  chemical reactions.
* You can look at the probability (according to one of possible
  statistical models) that progress (better quality of the positively
  reviewed sources or reviewing more sources to the same quality) can
  be made for one or multiple predicates with the trust metrics I
  have.
* Some sybil attack resistance is needed. In a system with no sybil
  resistance an attacker can clone an identity with only correct and
  structurally simple reviews and introduce one wrong one to make the
  review effort much less efficient. Structurally not simple but
  complex reviews could be flagged as suspicious by detecting
  duplication. One way to avoid such sybil attacks is to meet everyone
  in person before trusting them, however the cost of doing that with
  everyone would be a bottleneck for efficient review. So a mix of
  tools against sybil attacks is required.
* Both recording that a certain content should be forgotten with a
  given reason and availability of content that should not be
  forgotten are necessary.
* To allow conclusions like "nobody found an error in a source thus
  far", a way to show that one computer sees the same state of all
  reviews as others is needed. See
  https://gitlab.com/JanZerebecki/transparency-log-watcher for some
  notes on how this could be done.

Optional requirements
---------------------

* Time component for trust. It may be helpful for more efficient
  cooperation to split computation of trust for a person at a specific
  time or to compute it only on a time bracket instead of on all of
  their reviews.
* Inference of assertions. While this can be done manually or via
  bots, it may be more effective to calculate some inferences on every
  client.

Relationship with prediction markets
-------------------------------------

TODO
https://www.greaterwrong.com/tag/prediction-markets
https://en.wikipedia.org/wiki/Prediction_market

Advantages over naive reputation systems
----------------------------------------

* Differentiates between positive and negative references.
* Avoids incentive for not retracting what turned out to be wrong.
* Helps avoid repeating mistakes, so that mistakes that were not known
  as such in the past do not continue to get quoted as correct, even
  after they have been refuted.
* Works even though funny is not the same as correct.
* Works even though different people may understand a property like
  "correct" or "insightful" differently.
* Can cope with duplicate content

Comparison of implementations and similar systems
-------------------------------------------------

* rust-crev
